#!/usr/bin/env nextflow

/*

Lauch demultiplexing and quality check for Illumina data.

Usage:

NF_CONFIG // Nextflow configuration file (test or not)
RUNID // Run ID, e.g. 180328_A00302_0017_AH5HJTDMXX
*/

params.runID = '240308_A00302_0604_BH2J27DRX5'
//params.sequencer //NovaSeq, HiSeq, MiSeq
params.tmpDir = '/hpcscratch/ieo/bioinfogenomics/run_240308_A00302_0604_BH2J27DRX5/Results_18_03/240308_A00302_0604_BH2J27DRX5/Test_18_03'
//params.runDir 
tmpDirRunID = params.tmpDir + "/" + params.runID
//runDirSequencer = params.runDir + "/" + params.sequencer
params.publishDir = '/hpcscratch/ieo/bioinfogenomics/run_240308_A00302_0604_BH2J27DRX5/Results_18_03/240308_A00302_0604_BH2J27DRX5/Test_18_03/PublicData'
params.limsURL= 'https://samples.bioinfo-local.ieo.it'   
//params.recipientTrue
//params.recipientFalse

process printVersions {

    label 'pbs'

    publishDir "${tmpDirRunID}/${params.runID}"

    output:
    file "versions.txt"

    script:
    """
    echo "Process Name,Software,Version" > versions.txt
    echo "BCLCONVERT,bclconvert,00.000.000.4.2.7" >> versions.txt
    echo "CUSTOM_DUMPSOFTWAREVERSIONS,python,3.11.7" >> versions.txt
    echo "                            yaml,5.4.1" >> versions.txt
    echo "FALCO ,falco,1.2.1" >> versions.txt
    echo "FASTP,fastp,0.23.4" >> versions.txt
    echo "Workflow,Nextflow,23.10.1" >> versions.txt
    echo "         nf-core/demultiplex,1.4.1" >> versions.txt
    """
}

process prepareOutput {

    // Etichetta del processo
    label 'local'
    
    // Directory temporanea
    scratch '/hpcnfs/scratch/temporary/'

    errorStrategy 'retry'
    maxRetries = 1

    // Executor
    executor 'local'

    // Risorse
    cpus 1
    memory '1 G'

    // Input e output
    input:
    //file(fastp) from fastp_channel_two.toList()
    //path fastp 

    output:
    val 1

    // Script
    """
    # Creazione delle cartelle per i campioni e copia dei file fastq corrispondenti
    mkdir -p ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Fastqc
    tail -n +3 "${tmpDirRunID}/${params.runID}/SampleSheet-SM.csv" | while IFS=, read Lane Sample_ID Sample_Name index index2 Sample_Project Description Application user pi date organism group; do
        userOutput="${tmpDirRunID}/PublicData/\${pi}/\${user}/FASTQ/${params.runID}/Sample_\${Sample_ID}"
        mkdir -p \$userOutput
        # Assicurati che nessuno possa leggerlo
        find "${params.tmpDir}/${params.runID}/" -name "\${Sample_ID}*[RI][1-3]_[0-9][0-9][0-9].fastq.gz" -exec chmod 700 {} \\;
        find "${params.tmpDir}/${params.runID}/" -name "\${Sample_ID}*[RI][1-3]_[0-9][0-9][0-9].fastq.gz" -exec mv {} \$userOutput \\;
        find "${params.tmpDir}/${params.runID}/" -name "\${Sample_ID}*[RI][1-3]_[0-9][0-9][0-9]_fastp*" -exec cp {} \$userOutput \\;
        find "${params.tmpDir}/${params.runID}/" -name "\${Sample_ID}*[RI][1-3]_[0-9][0-9][0-9]*.md5" -exec cp {} \$userOutput \\;
        find "${params.tmpDir}/${params.runID}/" -name "\${Sample_ID}*[RI][1-3]_[0-9][0-9][0-9]_fastp*" -exec mv {} ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Fastqc \\;
        chmod a+w "${tmpDirRunID}/${params.runID}/versions.txt"
        cp "${tmpDirRunID}/${params.runID}/versions.txt" \$userOutput

        # Rimozione degli spazi non necessari negli indici
        index=\$(echo \$index | sed 's/\\s//g')
        index2=\$(echo \$index2 | sed 's/\\s//g')
    done
        
    # Copia dei file non identificati nella cartella di output degli indeterminati
    undeterminedOutput="${params.tmpDir}/PublicData/Undetermined/FASTQ/${params.runID}/"
    mkdir -p \$undeterminedOutput
    find "${params.tmpDir}/${params.runID}/" -name "Undetermined*_[RI][1-3]_[0-9][0-9][0-9]*.fastq.gz" -exec cp {} \$undeterminedOutput \\;
    find "${params.tmpDir}/${params.runID}/" -name "Undetermined*_[RI][1-3]_[0-9][0-9][0-9]*_fastp*"  -exec cp {} \$undeterminedOutput \\;
    find "${params.tmpDir}/${params.runID}/" -name "Undetermined*_[RI][1-3]_[0-9][0-9][0-9]*_fastp*"  -exec cp {} ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Fastqc \\;

    # Copia dei file non identificati nella cartella dell'utente quando il barcode è UNDETERMINED
    tail -n +3 "${tmpDirRunID}/${params.runID}/SampleSheet-SM.csv" | while IFS=, read Lane Sample_ID Sample_Name index index2 Sample_Project Description Application user pi date organism group; do
        index=\$(echo \$index | sed 's/\\s//g')
        index2=\$(echo \$index2 | sed 's/\\s//g')

        if [[ "\$index" == UNDETERMINED || "\$index2" == UNDETERMINED ]]; then
            if [[ ! -d "${tmpDirRunID}/PublicData/\${pi}/\${user}/FASTQ/${params.runID}/Undetermined/" ]]; then
                userUndeterminedOutput="${tmpDirRunID}/PublicData/\${pi}/\${user}/FASTQ/${params.runID}/Undetermined/"
                mkdir -p \$userUndeterminedOutput
            fi
            find "${tmpDirRunID}/PublicData/Undetermined/FASTQ/${params.runID}/" -name "Undetermined*_L00\$Lane*[RI][1-3]_[0-9][0-9][0-9]____*.fastq.gz" -exec cp {} \$userUndeterminedOutput \\;
            find "${tmpDirRunID}/PublicData/Undetermined/FASTQ/${params.runID}/" -name "Undetermined*_L00\$Lane*[RI][1-3]_[0-9][0-9][0-9]____*_fastp*" -exec cp {} \$userUndeterminedOutput \\;
        fi
    done
    
    # Copia delle cartelle contenenti i report e le statistiche
    mkdir -p ${tmpDirRunID}/PublicData/FASTQ/${params.runID}
    #chmod -R 777 "${tmpDirRunID}/Reports"
    cp -R "${tmpDirRunID}/Reports" ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/ 
    
    # Copia delle versioni e dei file MultiQC
    cp "${tmpDirRunID}/${params.runID}/versions.txt" ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/
    mkdir -p ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Multiqc
    cp "${params.tmpDir}/multiqc/multiqc_report.html" ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Multiqc/
    #chmod -R 777 "${params.tmpDir}/multiqc/multiqc_data"
    cp -R "${params.tmpDir}/multiqc/multiqc_data" ${tmpDirRunID}/PublicData/FASTQ/${params.runID}/Multiqc/

    # Esecuzione di publish.sh
    echo "publish"
    chmod +x ${tmpDirRunID}/${params.runID}/publish.sh
    sh ${tmpDirRunID}/${params.runID}/publish.sh
    """
}



process publishScript {

    
    label 'local'

    executor 'local'

    scratch '/hpcnfs/scratch/temporary/'

    publishDir "${tmpDirRunID}/${params.runID}", mode: 'move'

    
    output:
    file 'publish.sh' 
    file 'samplesinfo.csv' 
    """
    
    # Fastq dir for genomic unit
    echo "#copy genomic unit data" > publish.sh
    echo "chmod +w ${params.publishDir}" >> publish.sh
    echo "mkdir -p ${params.publishDir}/FASTQ" >> publish.sh
    echo "chmod +w ${params.publishDir}/FASTQ" >> publish.sh
    echo "cp -Lr ${tmpDirRunID}/PublicData/FASTQ/${params.runID} ${params.publishDir}/FASTQ/">> publish.sh
    echo "#Remove write permission">> publish.sh
    echo "chmod -w -R  ${params.publishDir}/FASTQ/${params.runID}">> publish.sh
    echo "chmod -w ${params.publishDir}/FASTQ">> publish.sh
    
    # get samples from info file
    # cp /pathToSamplesinfo/samplesinfo.csv .
    wget --no-check-certificate ${params.limsURL}/csv/flowcellinfo?runFolder=${params.runID} -O samplesinfo.csv
    cat samplesinfo.csv | tr -cd '\\11\\12\\15\\40-\\176' | sed "s/,/ /g" | sed "s/\\.xlsx//g"| cut -f 3,4,5 -d ' '|sort -u | while read user pi date; do
      echo "# Publish \$user">> publish.sh
      
      piPublishDir="${params.publishDir}/\$pi"
      
      echo "mkdir -p \$piPublishDir">> publish.sh
      echo "chmod +w \$piPublishDir">> publish.sh

      echo "mkdir -p \$piPublishDir/\$user">> publish.sh
      echo "chmod +w \$piPublishDir/\$user">> publish.sh
      echo "chmod -w \$piPublishDir">> publish.sh
      
      echo "mkdir -p \$piPublishDir/\$user/FASTQ">> publish.sh
      echo "chmod +w \$piPublishDir/\$user/FASTQ">> publish.sh
      echo "chmod -w \$piPublishDir/\$user">> publish.sh
      
      echo "mkdir -p \$piPublishDir/\$user/FASTQ/${params.runID}">> publish.sh
      echo "chmod +w \$piPublishDir/\$user/FASTQ/${params.runID}">> publish.sh
      echo "chmod -w \$piPublishDir/\$user/FASTQ">> publish.sh
      
      userOutput="${tmpDirRunID}/PublicData/\$pi/\$user/FASTQ/${params.runID}/"
     
      echo "cp -Lr \$userOutput/* \$piPublishDir/\$user/FASTQ/${params.runID}">> publish.sh
      echo "chmod -w -R \$piPublishDir/\$user/FASTQ/${params.runID}">> publish.sh

      echo ""
    done

    # Permissions
    echo "# Change permissions" >> publish.sh
    # cp /pathToSamplesheet/SampleSheet-SM.csv .
    curl --insecure ${params.limsURL}/csv/illuminasamplesheet?runFolder=${params.runID} |tr -cd '\\11\\12\\15\\40-\\176' > SampleSheet-SM.csv
        tail -n +3 "SampleSheet-SM.csv" | while IFS=, read Lane Sample_ID Sample_Name index index2 Sample_Project Description Application user pi date organism group; do
                piPublishDir="${params.publishDir}/\$pi"
        echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Sample_\$Sample_ID/ -name '*.fastq.gz' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chown {} bioinfo-genomics:\$group \\;">> publish.sh
        echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Sample_\$Sample_ID/ -name '*.fastq.gz' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 440 \\;">> publish.sh
        echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Sample_\$Sample_ID/ -name '*fastp.html' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 555 \\;">> publish.sh        
        echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Sample_\$Sample_ID/ -name '*fastp.json' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 555 \\;">> publish.sh

        #Remove unnecessary spaces in indexes
        index=\$(echo \$index | sed 's/\s//g')
        index2=\$(echo \$index2 | sed 's/\s//g')
        group=\$(echo \$group | sed 's/\r//')
        
        if [[ "\$index" == UNDETERMINED || "\$index2" == UNDETERMINED ]]; then
            echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Undetermined/ -name '*.fastq.gz' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chown {} bioinfo-genomics:\$group \\;">> publish.sh
            echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Undetermined/ -name '*.fastq.gz' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 440 \\;">> publish.sh
            echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Undetermined/ -name '*fastp.html' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 555 \\;">> publish.sh
            echo "find \$piPublishDir/\$user/FASTQ/${params.runID}/Undetermined/ -name '*fastp.json' -exec /usr/bin/sudo /hpcnfs/software/bin/chperm_safe chmod {} 555 \\;">> publish.sh 
        fi
        done

    echo "#Publish undetermined" >> publish.sh
    head -n 1 samplesinfo.csv | sed "s/,/ /g" | sed "s/\\.xlsx//g"| cut -f 3,4,5 -d ' ' |sort -u | while read user pi date; do
        undeterminedPublishDir="${params.publishDir}/Undetermined/FASTQ"
        undeterminedOutput="${tmpDirRunID}/PublicData/Undetermined/FASTQ/${params.runID}"
        
        echo "chmod +w \$undeterminedPublishDir">> publish.sh
        
        echo "cp -Lr  \$undeterminedOutput \$undeterminedPublishDir">> publish.sh
        
        echo "chmod -w -R  \$undeterminedPublishDir/${params.runID}">> publish.sh
        echo "chmod -w  \$undeterminedPublishDir">> publish.sh    
    done
        """
}


workflow {
    printVersions()  
    prepareOutput()
    publishScript()
}
